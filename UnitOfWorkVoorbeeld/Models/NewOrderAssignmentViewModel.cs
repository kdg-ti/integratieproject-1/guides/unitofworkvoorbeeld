﻿namespace UnitOfWorkVoorbeeld.Models;

public class NewOrderAssignmentViewModel
{
    public int OrderId { get; set; }
    public string CourierEmail { get; set; }
}