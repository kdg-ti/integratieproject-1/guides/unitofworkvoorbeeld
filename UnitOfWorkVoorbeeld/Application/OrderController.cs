﻿using Microsoft.AspNetCore.Mvc;
using UnitOfWorkVoorbeeld.BL.API;
using UnitOfWorkVoorbeeld.BL.Impl;
using UnitOfWorkVoorbeeld.Models;

namespace UnitOfWorkVoorbeeld.Application;

public class OrderController : Controller
{
    private readonly IOrderManager _orderManager;
    private readonly UnitOfWork _uow;

    public OrderController(IOrderManager orderManager, UnitOfWork uow)
    {
        _orderManager = orderManager;
        _uow = uow;
    }

    [HttpPost]
    public IActionResult AssignOrder(NewOrderAssignmentViewModel newOrderAssignmentViewModel)
    {
        _uow.BeginTransaction();
        var order = _orderManager.AssignOrder(newOrderAssignmentViewModel.OrderId, newOrderAssignmentViewModel.CourierEmail);
        _uow.Commit();
        return View("AssignOrder", order);
    }
}