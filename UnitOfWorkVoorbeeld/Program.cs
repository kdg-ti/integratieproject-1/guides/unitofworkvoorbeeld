using System.Diagnostics;
using Microsoft.EntityFrameworkCore;
using UnitOfWorkVoorbeeld.BL.API;
using UnitOfWorkVoorbeeld.BL.Impl;
using UnitOfWorkVoorbeeld.DAL.API;
using UnitOfWorkVoorbeeld.DAL.EF;
using UnitOfWorkVoorbeeld.DAL.Impl;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();


// Add services to the container.
builder.Services.AddDbContext<AppDbContext>(optionsBuilder => {
    optionsBuilder.UseSqlite("Data Source=app.sqlite");
    optionsBuilder.LogTo(msg => Debug.WriteLine(msg), LogLevel.Information);
}); // default 'Scoped' (= instance per http-request)
builder.Services.AddScoped<UnitOfWork>();

builder.Services.AddScoped<IOrderManager, OrderManager>();
builder.Services.AddScoped<IUserManager, UserManager>();
builder.Services.AddScoped<IOrderRepository, OrderRepository>();
builder.Services.AddScoped<IUserRepository, UserRepository>();

builder.Services.AddTransient<DbInitialiser>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

// Initialise the database
using var scope = app.Services.CreateScope();
var services = scope.ServiceProvider;
var initialiser = services.GetRequiredService<DbInitialiser>();
initialiser.Run();

app.Run();