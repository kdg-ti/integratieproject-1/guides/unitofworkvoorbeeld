﻿using System.ComponentModel.DataAnnotations;

namespace UnitOfWorkVoorbeeld.BL.Domain;

public class Courier
{
    [Key] public int CourierId { get; set; }

    public string Email { get; set; }

    public bool Available { get; set; }

    public void SetAvailable(bool available)
    {
        Available = available;
    }
}