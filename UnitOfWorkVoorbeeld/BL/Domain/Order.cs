﻿using System.ComponentModel.DataAnnotations;

namespace UnitOfWorkVoorbeeld.BL.Domain;

public class Order
{
    // OrderId (of Id) wordt automatisch als primary key gezien door Entity Framework
    public int OrderId { get; set; }
    public Courier Courier { get; set; }

    public IList<OrderEvent> OrderEvents { get; set; }

    public Order()
    {
        OrderEvents = new List<OrderEvent>();
    }

    public void SetStatus(OrderStatus status)
    {
        OrderEvents.Add(new OrderEvent(status));
    }
}