﻿using Microsoft.EntityFrameworkCore;

namespace UnitOfWorkVoorbeeld.BL.Domain;

public enum OrderStatus
{
    Registered,
    Assigned,
    InProgress,
    Delivered,
    Cancelled
}