﻿using System.ComponentModel.DataAnnotations;

namespace UnitOfWorkVoorbeeld.BL.Domain;

public class OrderEvent
{
    [Key] public int Id { get; set; }

    public OrderStatus Status { get; set; }

    public DateTime Time { get; set; }

    public OrderEvent(OrderStatus status)
    {
        Time = DateTime.Now;
        Status = status;
    }
}