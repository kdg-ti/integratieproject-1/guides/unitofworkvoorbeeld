﻿using UnitOfWorkVoorbeeld.BL.Domain;

namespace UnitOfWorkVoorbeeld.BL.API;

public interface IOrderManager
{
    public Order AssignOrder(int orderId, string courierEmail);

}