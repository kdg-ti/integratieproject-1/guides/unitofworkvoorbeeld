﻿using UnitOfWorkVoorbeeld.BL.Domain;

namespace UnitOfWorkVoorbeeld.BL.API;

public interface IUserManager
{
    public Courier GetCourier(string email);
    public void AssignCourierToOrder(Order order, string email);
}