﻿using UnitOfWorkVoorbeeld.BL.API;
using UnitOfWorkVoorbeeld.BL.Domain;
using UnitOfWorkVoorbeeld.DAL.API;

namespace UnitOfWorkVoorbeeld.BL.Impl;

public class UserManager : IUserManager
{
    private readonly IUserRepository _userRepository;

    public UserManager(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public Courier GetCourier(string courierEmail)
    {
        return _userRepository.GetCourier(courierEmail);
    }

    public void AssignCourierToOrder(Order order, string courierEmail)
    {
        var courier = _userRepository.GetCourier(courierEmail);
        order.Courier = courier;
        courier.SetAvailable(false);
    }
}