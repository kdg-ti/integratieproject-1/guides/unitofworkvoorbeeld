﻿using UnitOfWorkVoorbeeld.BL.API;
using UnitOfWorkVoorbeeld.BL.Domain;
using UnitOfWorkVoorbeeld.DAL.API;

namespace UnitOfWorkVoorbeeld.BL.Impl;

public class OrderManager : IOrderManager
{
    private readonly IUserManager _userManager;
    private readonly IOrderRepository _orderRepository;

    public OrderManager(IUserManager userManager, IOrderRepository orderRepository)
    {
        _userManager = userManager;
        _orderRepository = orderRepository;
    }

    public Order AssignOrder(int orderId, string courierEmail)
    {
        Order order = _orderRepository.GetOrder(orderId);
        _userManager.AssignCourierToOrder(order, courierEmail);
        order.SetStatus(OrderStatus.Assigned);
        _orderRepository.UpdateOrder(order);
        return order;
    }
}