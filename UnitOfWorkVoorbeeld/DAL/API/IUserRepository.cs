﻿using UnitOfWorkVoorbeeld.BL.Domain;

namespace UnitOfWorkVoorbeeld.DAL.API;

public interface IUserRepository
{
    Courier GetCourier(string courierId);
}