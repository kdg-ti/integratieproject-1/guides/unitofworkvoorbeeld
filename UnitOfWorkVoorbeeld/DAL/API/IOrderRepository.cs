﻿using UnitOfWorkVoorbeeld.BL.Domain;

namespace UnitOfWorkVoorbeeld.DAL.API;

public interface IOrderRepository
{
    Order GetOrder(int orderId);
    void UpdateOrder(Order order);
}