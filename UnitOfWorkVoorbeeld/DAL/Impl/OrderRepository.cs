﻿using Microsoft.EntityFrameworkCore;
using UnitOfWorkVoorbeeld.BL.Domain;
using UnitOfWorkVoorbeeld.DAL.API;
using UnitOfWorkVoorbeeld.DAL.EF;

namespace UnitOfWorkVoorbeeld.DAL.Impl;

public class OrderRepository : IOrderRepository
{
    private readonly AppDbContext _dbContext;

    public OrderRepository(AppDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public Order GetOrder(int orderId)
    {
        return _dbContext.Orders.Where(o => o.OrderId == orderId)
            .Include(o => o.OrderEvents).Include(o => o.Courier)
            .FirstOrDefault();
    }

    public void UpdateOrder(Order order)
    {
        _dbContext.Orders.Update(order);

        // we behouden hier toch de SaveChanges() zodat we de Repository
        // kunnen gebruiken zonder een UnitOfWork, bv in een test.
        _dbContext.SaveChanges();
    }
}