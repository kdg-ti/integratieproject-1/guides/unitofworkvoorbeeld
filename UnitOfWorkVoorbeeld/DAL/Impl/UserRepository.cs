﻿using UnitOfWorkVoorbeeld.BL.Domain;
using UnitOfWorkVoorbeeld.DAL.API;
using UnitOfWorkVoorbeeld.DAL.EF;

namespace UnitOfWorkVoorbeeld.DAL.Impl;

public class UserRepository : IUserRepository
{
    private readonly AppDbContext _appDbContext;

    public UserRepository(AppDbContext appDbContext)
    {
        _appDbContext = appDbContext;
    }

    public Courier GetCourier(string courierEmail)
    {
        return _appDbContext.Couriers.SingleOrDefault(c => c.Email == courierEmail);
    }
}