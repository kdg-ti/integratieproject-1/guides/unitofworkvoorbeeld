﻿using System.Diagnostics;
using Microsoft.EntityFrameworkCore;
using UnitOfWorkVoorbeeld.BL.Domain;

namespace UnitOfWorkVoorbeeld.DAL.EF;

public class AppDbContext : DbContext
{
    public DbSet<Order> Orders { get; set; }

    public DbSet<Courier> Couriers { get; set; }

    public DbSet<OrderEvent> OrderEvents { get; set; }

    public AppDbContext(DbContextOptions options) : base(options)
    {
    }
}