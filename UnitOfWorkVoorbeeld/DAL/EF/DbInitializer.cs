﻿using UnitOfWorkVoorbeeld.BL.Domain;

namespace UnitOfWorkVoorbeeld.DAL.EF;

public class DbInitialiser
{
    private readonly AppDbContext _context;

    public DbInitialiser(AppDbContext context)
    {
        _context = context;
    }

    public void Run()
    {
        Console.WriteLine("Initialising database...");
        _context.Database.EnsureDeleted();
        _context.Database.EnsureCreated();

        Seed(_context);
    }

    private void Seed(AppDbContext appDbContext)
    {
        var courier = new Courier
        {
            Available = true,
            Email = "karel.degrote@kdg.be"
        };
        appDbContext.Couriers.Add(courier);


        var order = new Order
        {
            OrderId = 1
        };
        
        order.SetStatus(OrderStatus.Registered);

        appDbContext.Orders.Add(order);

        appDbContext.SaveChanges();
    }
}